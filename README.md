# kimino-kaptcha
Fork of "@haileybot/captcha-generator" with a few customizations. Functionality is identical.
### Basic
```js
const Captcha = require("@haileybot/captcha-generator");
let captcha = new Captcha(width=270, height=60);

console.log(captcha.value);
```

### Save to File
```js
const path = require("path"),
const fs = require("fs");
const Captcha = require("kimino-kaptcha");
let captcha = new Captcha();

captcha.PNGStream.pipe(fs.createWriteStream(path.join(__dirname, `${captcha.value}.png`)));
captcha.JPEGStream.pipe(fs.createWriteStream(path.join(__dirname, `${captcha.value}.jpeg`)));
```

## License
This project is licensed under [GPL-3.0](https://github.com/s1nistr4/kimino-kaptcha/blob/main/LICENSE)
