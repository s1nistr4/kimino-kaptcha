import * as Canvas from "canvas";

Canvas.registerFont(require("path").resolve(__dirname, "../assets/ssp.ttf"), {family: "ssp"});

const randomText = (): string =>
		Math.random()
			.toString(36) // ?
			.replace(/[^a-z]|[gkqr]+/gi, "") // set what can be shown in the captcah
			.substring(0, 6) // max length of chars
			.toUpperCase(),
	shuffleArray = (arr: number[]): number[] => {
		let i: number = arr.length,
			temp: number,
			randomIndex: number;
		// While there remain elements to shuffle...
		while (0 !== i) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * i);
			i -= 1;
			// And swap it with the current element.
			temp = arr[i];
			arr[i] = arr[randomIndex];
			arr[randomIndex] = temp;
		}
		return arr;
	};

class Captcha {
	private _canvas: Canvas.Canvas;
	private _value: string;

	constructor(_h: number = 60) {
		// Make sure argument is a number, limit to a range from 250 to 400
		_h = typeof _h !== "number" || _h < 60 ? 60 : _h > 400 ? 400 : _h;

		// Initialize canvas
		this._canvas = Canvas.createCanvas(250, _h);
		const ctx = this._canvas.getContext("2d");

		// Set background color
		ctx.globalAlpha = 1;
		ctx.fillStyle = "#f9ebfa";
		ctx.beginPath();
		ctx.fillRect(0, 0, 250, _h); // fills in the background color
		ctx.save();

		ctx.lineWidth = 1;
		ctx.beginPath();
		const coords: number[][] = [];
		for (let i = 0; i < 8; i++) {
            // get a random line stroke
			if (!coords[i]) coords[i] = [];
			for (let j = 0; j < 5; j++) coords[i][j] = Math.round(Math.random() * 20) + j * 20;
			if (!(i % 2)) coords[i] = shuffleArray(coords[i]);
		};

		for (let i = 0; i < coords.length; i++) {
			if (!(i % 2)) {
				for (let j = 0; j < coords[i].length; j++) {
					if (!i) {
                        ctx.strokeStyle = ["#0000FF", "#00ff00", "#ff0000"][Math.floor(Math.random() * 3)];
						ctx.moveTo(coords[i][j], 40);
						ctx.lineTo(coords[i + 1][j], 240);
					} else {
                        ctx.strokeStyle = ["#0000FF", "#00ff00", "#ff0000"][Math.floor(Math.random() * 3)];
						ctx.moveTo(40, coords[i][j]);
						ctx.lineTo(240, coords[i + 1][j]);
					};
				};
			};
		};

		ctx.stroke();

		// Set style for text
		ctx.font = "bold 46px ssp";
		ctx.fillStyle = "#030425";

		// Set position for text
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";

		ctx.translate(0, _h);
		ctx.translate(
			Math.round(Math.random() * 100 - 50) + 150,
			-1 * Math.round(Math.random() * (_h / 4) - _h / 8) - _h / 2
		);
		// Set text value and print it to canvas
		ctx.beginPath();
		this._value = "";
		while (this._value.length !== 6) this._value = randomText();
		ctx.fillText(this._value, 0, 0);

		// Draw foreground noise
		ctx.restore();
		for (let i = 0; i < 5000; i++) {
			ctx.beginPath();
			let color = "#";
			while (color.length < 7) color += Math.round(Math.random() * 16).toString(16);
			color += "a0";
			ctx.fillStyle = color;
			ctx.arc(
				Math.round(Math.random() * 0), // X coordinate
				Math.round(Math.random() * _h), // Y coordinate
				Math.random() * 2, // Radius
				0, // Start angle
				Math.PI * 2 // End angle
			);
			ctx.fill();
		}
	}

	get value(): string {
		return this._value;
	}

	get PNGStream(): Canvas.PNGStream {
		return this._canvas.createPNGStream();
	}

	get JPEGStream(): Canvas.JPEGStream {
		return this._canvas.createJPEGStream();
	}

	get dataURL(): string {
		return this._canvas.toDataURL("image/jpeg");
	}
}

export = Captcha;
